#!/bin/sh
_sdir="$(dirname "$(readlink -f "$0")")/ints"
_dir="${1:-$_sdir}"
_begin="${2:-0}"
_end="${3:-245}"

echo "You can optionally provide the int directory as \$1, start as \$2"
echo "and end as \$3. The latter two options should be provided as integers"
echo
echo "start: $_dir/F$_begin"
echo "end:   $_dir/F$_end"
echo
echo "This can take quite a while to complete."
echo "Hit enter to continue or ^C to abort."

read F

echo "=== Generating VByte files ==="
for i in $(seq "$_begin" "$_end") ; do
	echo -n "F${i}: enc"
	./vbyte encode "${_dir}"/F"$i"
	echo -n " dec"
	./vbyte decode "${_dir}"/F"$i".vb
	echo -n " sort"
	./vbyte sort   "${_dir}"/F"$i"
	echo " done."
done
