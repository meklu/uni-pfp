#!/bin/sh
_sdir="$(dirname "$(readlink -f "$0")")/ints"
_dir="${1:-$_sdir}"
_begin="${2:-0}"
_end="${3:-245}"

echo "Run generate_vbs.sh first!"
echo "You can optionally provide the int directory as \$1, start as \$2"
echo "and end as \$3. The latter two options should be provided as integers"
echo
echo "start: $_dir/F$_begin"
echo "end:   $_dir/F$_end"
echo
echo "Hit enter to continue or ^C to abort."

read F

_fail=0

echo "=== ENCODE === Encode/decode sanity checking via enc/dec round-trip"
_count=0
for i in $(seq "$_begin" "$_end") ; do
	_a="$(md5sum "${_dir}"/F"$i"        | awk '{print $1}')"
	_b="$(md5sum "${_dir}"/F"$i".vb.dec | awk '{print $1}')"
	if [ x"$_a" != x"$_b" ]; then
		_fail=1
		_count=$(($_count + 1))
	fi
done
echo "${_count} failures"

echo "===  SORT  === Sort functionality checking"
_count=0
for i in $(seq "$_begin" "$_end") ; do
	_a="$(./vbyte printv "${_dir}"/F"$i".vb        | sort -n                                   | md5sum | awk '{print $1}')"
	_b="$(./vbyte printv "${_dir}"/F"$i".sorted.vb | awk 'BEGIN { acc=0; } { print acc+=$1; }' | md5sum | awk '{print $1}')"
	if [ x"$_a" != x"$_b" ]; then
		_fail=1
		_count=$(($_count + 1))
	fi
done
echo "${_count} failures"

if [ x"$_fail" = x"1" ]; then
	exit 1
fi
