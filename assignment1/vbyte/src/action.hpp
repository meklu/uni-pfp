#pragma once

#ifndef _ACTION_HPP
#define _ACTION_HPP

#include <cstdint>
#include <string>

namespace action {

int printv(std::string infile);
int encode(std::string infile);
int decode(std::string infile);
int sort(std::string infile);

int proxis(std::string infile, uint64_t lower_bound, uint64_t upper_bound);

}

#endif // _ACTION_HPP
