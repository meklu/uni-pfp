#include <iostream>

#include "action.hpp"
#include "file.hpp"
#include "vbyte.hpp"

int action::decode(std::string infile) {
	std::string outfile = infile + ".dec";

	std::ifstream ifs = file::in(infile);
	if (!ifs.is_open()) {
		std::cout << "Couldn't open input file!" << std::endl;
		return 1;
	}
	std::ofstream ofs = file::out(outfile);
	if (!ofs.is_open()) {
		std::cout << "Couldn't open output file!" << std::endl;
		return 1;
	}
	std::vector<uint8_t> ns = file::read_full_u8(ifs);
	uint8_t *p = (uint8_t *) ns.data();
	uint8_t *end = p + ns.size();
	while (p < end) {
		uint64_t dec = vbyte::decode(&p, end);
		for (int i = 0; i < 8; ++i) {
			ofs.put(dec & 0xff);
			dec >>= 8;
		}
	}
	ifs.close();
	ofs.close();
	return 0;
}
