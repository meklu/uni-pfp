#include <iostream>

#include "action.hpp"
#include "file.hpp"
#include "vbyte.hpp"

int action::printv(std::string infile) {
	std::ifstream ifs = file::in(infile);
	if (!ifs.is_open()) {
		std::cout << "Couldn't open input file!" << std::endl;
		return 1;
	}
	std::vector<uint8_t> ns = file::read_full_u8(ifs);
	uint8_t *p = (uint8_t *) ns.data();
	uint8_t *end = p + ns.size();
	while (p < end) {
		uint64_t dec = vbyte::decode(&p, end);
		printf("%llu\n", (unsigned long long) dec);
	}
	ifs.close();
	return 0;
}
