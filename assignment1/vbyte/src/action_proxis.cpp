#include <chrono>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

#include "action.hpp"
#include "file.hpp"
#include "vbyte.hpp"

std::chrono::steady_clock::duration readNs(std::ifstream & file, std::set<uint64_t> & ns) {
	std::chrono::steady_clock::time_point f1 = std::chrono::steady_clock::now();
	std::vector<uint8_t> bs = file::read_full_u8(file);
	std::chrono::steady_clock::time_point f2 = std::chrono::steady_clock::now();

	uint8_t *p = (uint8_t *) bs.data();
	uint8_t *end = p + bs.size();
	while (p < end) {
		uint64_t dec = vbyte::decode(&p, end);
		ns.insert(dec);
	}

	return f2 - f1;
}

int action::proxis(std::string infile, uint64_t lower_bound, uint64_t upper_bound) {
	std::string dir = ".";
	{
		size_t slashpos = infile.rfind('/');
		if (slashpos != std::string::npos) {
			dir = infile.substr(0, slashpos);
		}
	}
#if DEBUG == 1
	std::cout << "dir: " << dir << std::endl;
#endif

	std::ifstream ifs = file::in(infile);
	if (!ifs.is_open()) {
		std::cout << "Couldn't open input file!" << std::endl;
		return 1;
	}

	ifs.seekg(0, std::ios_base::beg);

	std::vector<std::pair<uint64_t, uint64_t>> pairs;
	{
		std::string buf;
		uint64_t a, b;
		while (!ifs.eof()) {
			size_t pos = 0;
			std::getline(ifs, buf);
			if (ifs.fail()) { break; }
			a = std::stoull(buf.c_str(), &pos);
			buf = buf.substr(pos+1);
			b = std::stoull(buf.c_str());

			pairs.push_back(std::pair<uint64_t, uint64_t>(a, b));
		}
	}
	ifs.close();

	std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
	std::chrono::steady_clock::duration fs_use;
	fs_use = fs_use.zero();

	for (uint64_t i = 0; i < pairs.size(); ++i) {
#if DEBUG == 1
		std::cout << "a: " << std::get<0>(pairs[i]) << ", b: " << std::get<1>(pairs[i]) << std::endl;
#endif
		std::chrono::steady_clock::time_point f1 = std::chrono::steady_clock::now();

		std::string   afn = dir + "/F" + std::to_string(std::get<0>(pairs[i])) + ".vb";
		std::ifstream afs = file::in(afn);
		if (!afs.is_open()) {
			std::cout << "failed to open " << afn << std::endl;
			return 1;
		}
		std::string   bfn = dir + "/F" + std::to_string(std::get<1>(pairs[i])) + ".vb";
		std::ifstream bfs = file::in(bfn);
		if (!bfs.is_open()) {
			std::cout << "failed to open " << bfn << std::endl;
			return 1;
		}

		std::chrono::steady_clock::time_point f2 = std::chrono::steady_clock::now();
		fs_use += f2 - f1;

		std::set<uint64_t> a, b;
		fs_use += readNs(afs, a);
		fs_use += readNs(bfs, b);
		afs.close();
		bfs.close();

		uint64_t sz = 0;
		// for each of element of set b, check whether set a has any element that satisfies both boundaries
		for (uint64_t e : b) {
			// set::lower_bound has O(log n) time complexity, so it's probably implemented as a binary search
			// on the set elements which should be in order in memory since there also exists an unsorted_set
			// template in STL
			std::set<uint64_t>::iterator lit = a.lower_bound(e - lower_bound);
			if (lit == a.end()) {
				// nothing satisfies the lower bound
				continue;
			}
			if (*lit <= e + upper_bound) {
				// satisfies both bounds
				++sz;
			}
		}
		std::cout << sz << std::endl;
	}

	std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
	std::chrono::steady_clock::duration dt = t2 - t1;
	std::chrono::duration<double> dtd = std::chrono::duration_cast<std::chrono::duration<double>>(dt);
	std::chrono::duration<double> dtf = std::chrono::duration_cast<std::chrono::duration<double>>(dt - fs_use);

	std::cout << "Total duration: " << dtd.count() << " s" << std::endl;
	std::cout << "Sans file ops:  " << dtf.count() << " s" << std::endl;

	return 0;
}
