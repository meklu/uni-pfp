#include <iostream>

#include "action.hpp"
#include "file.hpp"
#include "sort.hpp"
#include "vbyte.hpp"

int action::sort(std::string infile) {
	std::string outfile = infile + ".sorted.vb";

	std::ifstream ifs = file::in(infile);
	if (!ifs.is_open()) {
		std::cout << "Couldn't open input file!" << std::endl;
		return 1;
	}
	std::ofstream ofs = file::out(outfile);
	if (!ofs.is_open()) {
		std::cout << "Couldn't open output file!" << std::endl;
		return 1;
	}
	std::vector<uint64_t> ns = file::read_full_u64(ifs);
	sort::sort(ns);
	uint64_t last = 0;
	for (uint64_t i = 0; i < ns.size(); ++i) {
		std::vector<uint8_t> vb = vbyte::encode(ns[i] - last);
		ofs.write((char *) vb.data(), vb.size());
		last = ns[i];
	}
	ifs.close();
	ofs.close();
	return 0;
}
