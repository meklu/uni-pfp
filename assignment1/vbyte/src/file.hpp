#pragma once

#ifndef _FILE_HPP
#define _FILE_HPP

#include <cstdint>
#include <fstream>
#include <vector>

namespace file {

std::ofstream out(const std::string & filename);
std::ifstream in(const std::string & filename);

std::vector<uint64_t> read_full_u64(std::ifstream & f);
std::vector<uint8_t> read_full_u8(std::ifstream & f);

}

#endif // _FILE_HPP
