#include <iostream>
#include <cstring>

#include <cerrno>
#include <sys/time.h>
#include <sys/resource.h>

#include "action.hpp"
#include "vbyte.hpp"

struct timespec ts_start;

void print_resource_report() {
	struct rusage r;
	struct timespec ts_end;
	errno = 0;
	if (0 > getrusage(RUSAGE_SELF, &r)) {
		std::cout << "getrusage failed: " << strerror(errno) << std::endl;
		return;
	}
	clock_gettime(CLOCK_MONOTONIC, &ts_end);
	ts_end.tv_nsec -= ts_start.tv_nsec;
	if (ts_end.tv_nsec < 0) {
		ts_end.tv_nsec += 1000000000;
		ts_end.tv_sec -= 1;
	}
	ts_end.tv_sec -= ts_start.tv_sec;
	printf("--- resource usage ---\n");
	printf("max rss: %ldK\n", r.ru_maxrss);
	printf("wtime  : %lld.%09lld\n",
		(long long) ts_end.tv_sec,
		(long long) ts_end.tv_nsec
	);
	printf("utime  : %lld.%06lld\n", (long long) r.ru_utime.tv_sec, (long long) r.ru_utime.tv_usec);
	printf("stime  : %lld.%06lld\n", (long long) r.ru_stime.tv_sec, (long long) r.ru_stime.tv_usec);
}

void pu8(uint8_t u8) {
	printf("%hhx%hhx%hhx%hhx%hhx%hhx%hhx%hhx\n", (u8 >> 7) & 1, (u8 >> 6) & 1, (u8 >> 5) & 1, (u8 >> 4) & 1, (u8 >> 3) & 1, (u8 >> 2) & 1, (u8 >> 1) & 1, (u8 >> 0) & 1);
}

#if DEBUG == 1

#include "sort.hpp"

void old() {
	std::cout << "Enc" << std::endl;
	std::vector<uint8_t> a = vbyte::encode(1 << 10);
	for (int i = 0; ; ++i) {
		pu8(a[i]);
		if (a[i] & 0x80) { break; }
	}
	std::cout << "Dec" << std::endl;
	uint8_t b[] = {0, 0x88, 0x81};
	uint8_t *p = (uint8_t *) (&b);
	uint64_t be;
	be = vbyte::decode(&p, &(b[2]));
	for (int i = 0; i < 2; ++i) {
		pu8((uint8_t) (((be >> (i*8)) & 0xff)));
	}
	std::cout << p - b << std::endl;
	be = vbyte::decode(&p, &(b[2]));
	for (int i = 0; i < 1; ++i) {
		pu8((uint8_t) (((be >> (i*8)) & 0xff)));
	}
	std::cout << "Sorterino" << std::endl;
	std::vector<int64_t> v({0, 4, 2, 0, 8, -2, 16});
	sort::sort(v);
	for (uint64_t i = 0; i < v.size(); ++i) {
		std::cout << v[i] << std::endl;
	}
}
#endif

void usage(const char *argv0) {
	std::cout << "USAGE: " << argv0 << " <action> <arguments>" << std::endl;
	std::cout << "Actions:" << std::endl;
#if DEBUG == 1
	std::cout << "\t" << "debug" << std::endl;
	std::cout << "\t\t" << "Flavor of the hour development stuff" << std::endl;
#endif
	std::cout << "\t" << "printv <input_file>" << std::endl;
	std::cout << "\t\t" << "Prints a VByte encoded file's integers in decimal form" << std::endl;
	std::cout << "\t" << "encode <input_file>" << std::endl;
	std::cout << "\t\t" << "Encodes into VByte sequences a file consisting of 64-bit unsigned integers" << std::endl;
	std::cout << "\t" << "decode <input_file>" << std::endl;
	std::cout << "\t\t" << "Decodes a file consisting of one or more VByte sequences" << std::endl;
	std::cout << "\t" << "sort   <input_file>" << std::endl;
	std::cout << "\t\t" << "Sorts a set of integers and encodes adjacent deltas as VByte sequences" << std::endl;
	std::cout << "\t" << "proxis <input_file> <lower> <upper>" << std::endl;
	std::cout << "\t\t" << "Calculates proximity intersection for the sets given in <input_file>" << std::endl;
	std::cout << "\t\t" << "The .vb files accessed will be assumed to reside in the same directory as" << std::endl;
	std::cout << "\t\t" << "the input file." << std::endl;
}

int main(int argc, char **argv) {
	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	if (argc <= 2) {
#if DEBUG == 1
		if (argc == 2 && !strncmp("debug", argv[1], 6)) {
			old();
			return 0;
		}
#endif
		std::cout << "Too few arguments supplied!" << std::endl;
		usage(argv[0]);
		return 1;
	}
	if (argc > 3) {
		if (argc == 5 && !strncmp("proxis", argv[1], 7)) {
			uint64_t lower, upper;
			try {
				lower = std::stoull(argv[3]);
				upper = std::stoull(argv[4]);
			} catch (...) {
				std::cout << "Failed to parse number parameters!" << std::endl;
				return 1;
			}
			int ret = action::proxis(argv[2], lower, upper);
			print_resource_report();
			return ret;
		}
		std::cout << "Too many arguments supplied!" << std::endl;
		usage(argv[0]);
		return 1;
	}
	if (!strncmp("encode", argv[1], 7)) {
		int ret = action::encode(argv[2]);
		print_resource_report();
		return ret;
	} else if (!strncmp("decode", argv[1], 7)) {
		int ret = action::decode(argv[2]);
		print_resource_report();
		return ret;
	} else if (!strncmp("printv", argv[1], 7)) {
		int ret = action::printv(argv[2]);
		print_resource_report();
		return ret;
	} else if (!strncmp("sort", argv[1], 5)) {
		int ret = action::sort(argv[2]);
		print_resource_report();
		return ret;
	}
	return 0;
}
