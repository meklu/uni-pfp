// This is a simple templated implementation of merge sort
//
// Usage:
// #include "sort.hpp"
//    std::vector<uint8_t> somevector;
//    populate(somevector);
//    sort::sort(somevector);

#pragma once

#ifndef _SORT_HPP
#define _SORT_HPP

#include <vector>

namespace sort {

// internal methods not for direct access
// this anonymous namespace is very convenient for making these effectively private
namespace {
	template <class T> void merge(std::vector<T> & tmp, std::vector<T> & vec, uint64_t a1, uint64_t b1, uint64_t a2, uint64_t b2) {
		uint64_t beg = a1;
		uint64_t put = beg;
		uint64_t end = b2;
		while (put <= end) {
			if (a2 > b2 || (a1 <= b1 && vec[a1] <= vec[a2])) {
				tmp[put] = vec[a1];
				++a1;
			} else {
				tmp[put] = vec[a2];
				++a2;
			}
			++put;
		}
		for (put = beg; put <= end; ++put) {
			vec[put] = tmp[put];
		}
	}

	template <class T> void sort(std::vector<T> & tmp, std::vector<T> & vec, uint64_t a, uint64_t b) {
		if (a == b) {
			return;
		}
		uint64_t k = (b-a) / 2 + a;
		sort(tmp, vec, a, k);
		sort(tmp, vec, k+1, b);
		merge(tmp, vec, a, k, k+1, b);
	}
}

template <class T> void sort(std::vector<T> & vec) {
	std::vector<T> tmp = std::vector<T>(vec.size());
	sort(tmp, vec, 0, vec.size()-1);
}

}

#endif // _SORT_HPP
