#include "vbyte.hpp"

uint64_t vbyte::decode(uint8_t **p, const uint8_t *end) {
	if (!p) {
		return 0;
	}
	uint8_t *inp = *p;
	if (!inp) {
		return 0;
	}
	uint64_t ret = 0;
	int byte_count = 0;
	bool stop = 0;
	// a vbyte will at most take up 10 bytes in the input stream, as a
	// 64-bit payload will take ceil(64/7) = 10 bytes of space
	// (0xffffffffffffffff)
	while (!stop && inp <= end && byte_count < 10) {
		if (0x80 & *inp) {
			stop = 1;
		}
		ret |= (*inp & 0x7f) << (byte_count * 7);
		++byte_count;
		++inp;
	}
	*p = inp;
	return ret;
}

std::vector<uint8_t> vbyte::encode(uint64_t enc) {
	// figure out how many bytes we'll need
	int bytes_needed = 0;
	{
		uint64_t h = enc;
		// the condition is at the arse end because even a zero will
		// need a single byte
		do {
			++bytes_needed;
			h >>= 7;
		} while (h);
	}
	std::vector<uint8_t> vb = std::vector<uint8_t>(bytes_needed);
	for (int i = 0; enc; ++i) {
		vb[i] = enc & 0x7f;
		enc >>= 7;
	}
	// set the stop bit
	vb[bytes_needed-1] |= 0x80;
	return vb;
}

