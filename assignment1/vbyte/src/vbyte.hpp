#pragma once

#ifndef _VBYTE_H
#define _VBYTE_H

#include <cstdint>
#include <vector>

namespace vbyte {

// p should be the memory location of your input pointer
//   upon return it'll contain the location of the next vbyte sequence to
//   decode
// end denotes the final memory address that is available for decoding
//   note that if this doesn't contain an octet with a stop bit, you'll get a
//   weird partially decoded result
uint64_t decode(uint8_t **p, const uint8_t *end);

// pretty simple, returns a vbyte sequence for a given integer
std::vector<uint8_t> encode(uint64_t enc);

}

#endif // _VBYTE_H
