DEBUG := 0
CXXFLAGS := $(CXXFLAGS) -std=c++14 -Wall -Wextra -Wshadow -pedantic -Os
LDFLAGS := $(LDFLAGS)

ifeq ($(DEBUG), 1)
	CPPFLAGS := $(CPPFLAGS) -DDEBUG=1
	CXXFLAGS := $(CXXFLAGS) -g -pg
	LDFLAGS := $(LDFLAGS) -pg
endif

SRC := \
	main.cpp \
	action_bitarr.cpp \
	action_pkint.cpp \
	bitarray.cpp \
	packedintegerarray.cpp

TARGETS := bitsies

SRC := $(addprefix src/, $(SRC))
OBJ := $(SRC:%.cpp=%.o)
DEP := $(OBJ:%.o=%.d)

.PHONY : all fall clean

all : $(TARGETS)

fall :
	$(MAKE) clean
	$(MAKE) all

clean :
	$(RM) $(OBJ)
	$(RM) $(DEP)
	$(RM) $(TARGETS)

%.o : %.c
	$(CXX) $(CXXFLAGS) -MD -c $< -o $@
	@cp -f $*.d $*.d.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@$(RM) $*.d.tmp

bitsies : $(OBJ)
	$(CXX) $(CFLAGS) $^ -o $@ $(LDFLAGS)

-include $(DEP)
