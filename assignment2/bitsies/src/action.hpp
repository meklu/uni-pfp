#pragma once

#ifndef _PFP_ACTION_HPP
#define _PFP_ACTION_HPP

#include <cstdint>

namespace action {

int bitarr(uint64_t n, uint64_t seed);
int bitarr(uint64_t n);

int pkint(uint64_t n, uint64_t k, uint64_t seed);
int pkint(uint64_t n, uint64_t k);

}

#endif // _PFP_ACTION_HPP
