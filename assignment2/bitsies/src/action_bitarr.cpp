#include <chrono>
#include <iostream>
#include <cstdio>
#include <vector>
#include <random>

#include "action.hpp"
#include "bitarray.hpp"

template <class T> void p_delta(
	const std::string & str,
	std::chrono::time_point<T> ts_start,
	std::chrono::time_point<T> ts_end
) {
	std::chrono::nanoseconds elapsed = ts_end - ts_start;
	// This is kind of ugly but there were some issues doing it supposedly nicely
	std::chrono::nanoseconds s = elapsed / 1000000000ULL;
	std::chrono::nanoseconds ns = elapsed - s;
	printf(
		"%s: %llu.%09llu s\n",
		str.c_str(),
		(unsigned long long) s.count(),
		(unsigned long long) ns.count()
	);
}

int action::bitarr(uint64_t n, uint64_t seed) {
	std::mt19937_64 gen(seed);
	// this is an inclusive range, hence -1
	std::uniform_int_distribution<> dis(0, n-1);

	bitarray::BitArray *ba = new bitarray::BitArray(n);
	uint64_t m = n / 2;
	std::vector<uint64_t> v(m);
	for (uint64_t i = 0; i < m; ++i) {
		uint64_t p = dis(gen);
#if DEBUG == 1
		std::cout << "p: " << p << std::endl;
#endif
		v.push_back(p);
	}
	{
		auto ts_start = std::chrono::steady_clock::now();
		for (uint64_t i = 0; i < m; ++i) {
			ba->set(v[i], 1);
		}
		auto ts_end = std::chrono::steady_clock::now();
		p_delta("set ", ts_start, ts_end);
	}
	{
		auto ts_start = std::chrono::steady_clock::now();
		for (uint64_t i = 0; i < m; ++i) {
			ba->get(v[i]);
		}
		auto ts_end = std::chrono::steady_clock::now();
		p_delta("get ", ts_start, ts_end);
	}
	v.clear();
	{
		for (uint64_t i = 0; i < m; ++i) {
			v.push_back(dis(gen));
		}
		auto ts_start = std::chrono::steady_clock::now();
		for (uint64_t i = 0; i < m; ++i) {
			ba->get(v[i]);
		}
		auto ts_end = std::chrono::steady_clock::now();
		p_delta("get2", ts_start, ts_end);
	}
	fflush(stdout);
#if DEBUG == 1
	ba->p();
#endif
	delete ba;
	return 0;
}

int action::bitarr(uint64_t n) {
	std::random_device rd;
	return action::bitarr(n, rd());
}
