#include <chrono>
#include <iostream>
#include <cstdio>
#include <vector>
#include <random>

#include "action.hpp"
#include "packedintegerarray.hpp"

template <class T> void p_delta(
	const std::string & str,
	std::chrono::time_point<T> ts_start,
	std::chrono::time_point<T> ts_end
) {
	std::chrono::nanoseconds elapsed = ts_end - ts_start;
	// This is kind of ugly but there were some issues doing it supposedly nicely
	std::chrono::nanoseconds s = elapsed / 1000000000ULL;
	std::chrono::nanoseconds ns = elapsed - s;
	printf(
		"%s: %llu.%09llu s\n",
		str.c_str(),
		(unsigned long long) s.count(),
		(unsigned long long) ns.count()
	);
}

int action::pkint(uint64_t n, uint64_t k, uint64_t seed) {
	std::mt19937_64 gen(seed);
	uint64_t max = k < sizeof(k)*8 ? (1LL << k) - 1LL : ~0LL;
	std::uniform_int_distribution<uint64_t> dis(0, max);

	packedintegerarray::PackedIntegerArray *pia = new packedintegerarray::PackedIntegerArray(n, k);
	std::vector<uint64_t> v(n);
	for (uint64_t i = 0; i < n; ++i) {
		uint64_t p = dis(gen);
#if DEBUG == 1
		std::cout << "p: " << p << std::endl;
#endif
		v[i] = p;
	}
	{
		auto ts_start = std::chrono::steady_clock::now();
		for (uint64_t i = 0; i < n; ++i) {
			pia->set(i, v[i]);
		}
		auto ts_end = std::chrono::steady_clock::now();
		p_delta("set ", ts_start, ts_end);
	}
	for (uint64_t i = 0; i < n; ++i) {
		if (pia->get(i) != v[i]) {
			std::cout << "Value mismatch!" << std::endl;
			std::cout << "\tpia: " << pia->get(i) << ", v: " << v[i] << std::endl;
			delete pia;
			return 1;
		}
	}
	std::cout << "Success." << std::endl;
	fflush(stdout);
	delete pia;
	return 0;
}

int action::pkint(uint64_t n, uint64_t k) {
	std::random_device rd;
	return action::pkint(n, k, rd());
}
