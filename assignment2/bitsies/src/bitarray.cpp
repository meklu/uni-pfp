#include "bitarray.hpp"

#include <iostream>

using namespace bitarray;

BitArray::BitArray(uint64_t n) {
	this->_size = n;
	this->_d = new uint64_t[1+n/64];
	for (uint64_t i = 0; i < 1+n/64; ++i) {
		this->_d[i] = 0LL;
	}
	this->_sums = NULL;
	// calculate _tmask
	// since our general case might fail when _t equals the bit count of our
	// mask data type, we do something clever instead
	this->_tmask = this->_t < sizeof(this->_tmask)*8 ? (1LL << this->_t) - 1LL : ~0LL;
#if DEBUG == 1
	std::cout << "t = " << this->_t << ", tispow2 = " << this->_tispow2 << std::endl;
#endif
}

BitArray::~BitArray() {
	delete [] this->_d;
	delete [] this->_sums;
}

uint64_t BitArray::get(uint64_t i) {
	if (i > this->_size) { return 0LL; }
	return (this->_d[i/64] >> i%64) & 1LL;
}

void BitArray::set(uint64_t i, uint8_t b) {
	if (i > this->_size) { return; }
	if (b) {
		this->_d[i/64] |= 1LL << (i%64);
	} else {
		this->_d[i/64] &= ~(1LL << (i%64));
	}
}

uint64_t BitArray::size() {
	return this->_size;
}

void BitArray::calcSums() {
	uint64_t blocks = 1+this->_size / this->_t;
	this->_sums = new uint64_t[blocks];
	if (!this->_sums) {
		std::cout << "Failed to allocate sum support structure!" << std::endl;
		return;
	}
	uint64_t sum = 0;
	uint64_t off = 0LL;
	for (uint64_t i = 0, di = 0; i < blocks; ++i) {
		this->_sums[i] = sum;
		sum += __builtin_popcountll(this->_d[di] & (this->_tmask << off));
		if (off > 64 - this->_t) {
			sum += __builtin_popcountll(this->_d[di+1] & (this->_tmask >> (this->_t - (64 - off))));
		}
		off += this->_t;
		if (off >= 64) {
			++di;
		}
		off %= 64;
	}
}

uint64_t BitArray::sum(uint64_t i) {
	if (i >= this->_size) { return 0LL; }
	uint64_t tr = (i / this->_t) * this->_t;
	uint64_t ret = this->_sums[i/this->_t];
	uint64_t chk = this->_d[tr/64];
	uint8_t lead = 64 - __builtin_clzll(chk);
	uint8_t trail = __builtin_ctzll(chk);
	if (tr % 64 > trail) {
		trail = tr % 64;
	}
	for (uint64_t j = trail; j < lead; ++j) {
		ret += (chk >> j) & 1LL;
	}
	if (tr/64 < i/64) {
		chk = this->_d[i/64];
		lead = 64 - __builtin_clzll(chk);
		if (1 + (i % 64) < lead) {
			lead = 1 + (i % 64);
		}
		trail = __builtin_ctzll(chk);
		for (uint64_t j = trail; j < lead; ++j) {
			ret += (chk >> j) & 1LL;
		}
	}
	return ret;
}

void BitArray::p() {
	uint64_t i;
	for (i = this->_size - 1;; --i) {
		std::cout << this->get(i);
		if (i == 0) {
			break;
		}
	}
	std::cout << std::endl;
}
