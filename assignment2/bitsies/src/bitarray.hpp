#pragma once

#ifndef _PFP_BITARRAY_HPP
#define _PFP_BITARRAY_HPP

#include <cstdint>

namespace bitarray {

class BitArray {
private:
	uint64_t _size;
	uint64_t *_d;
	uint64_t *_sums;
	// segment length of sum support structure, max. 64
	const uint64_t _t = 64;
	const bool _tispow2 = __builtin_popcountll(_t) == 1;
	uint64_t _tmask = 0;

public:
	BitArray(uint64_t n);
	~BitArray();

	// Gets bit at position i
	uint64_t get(uint64_t i);
	// Sets bit at position i
	void set(uint64_t i, uint8_t b);

	// Returns the size of our BitArray in bits
	uint64_t size();

	// Calculates the sum support structure in O(n) time
	void calcSums();
	// Gets the sum of set bits up to position i
	uint64_t sum(uint64_t i);

	// Prints a tidy version of our BitArray in binary form
	void p();
};

}

#endif // _PFP_BITARRAY_HPP
