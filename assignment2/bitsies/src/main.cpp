#include <iostream>
#include <cstring>

#include <cerrno>
#include <sys/time.h>
#include <sys/resource.h>

#include "action.hpp"
#include "bitarray.hpp"

struct timespec ts_start;

void print_resource_report() {
	struct rusage r;
	struct timespec ts_end;
	errno = 0;
	if (0 > getrusage(RUSAGE_SELF, &r)) {
		std::cout << "getrusage failed: " << strerror(errno) << std::endl;
		return;
	}
	clock_gettime(CLOCK_MONOTONIC, &ts_end);
	ts_end.tv_nsec -= ts_start.tv_nsec;
	if (ts_end.tv_nsec < 0) {
		ts_end.tv_nsec += 1000000000;
		ts_end.tv_sec -= 1;
	}
	ts_end.tv_sec -= ts_start.tv_sec;
	printf("--- resource usage ---\n");
	printf("max rss: %ldK\n", r.ru_maxrss);
	printf("wtime  : %lld.%09lld\n",
		(long long) ts_end.tv_sec,
		(long long) ts_end.tv_nsec
	);
	printf("utime  : %lld.%06lld\n", (long long) r.ru_utime.tv_sec, (long long) r.ru_utime.tv_usec);
	printf("stime  : %lld.%06lld\n", (long long) r.ru_stime.tv_sec, (long long) r.ru_stime.tv_usec);
}

void pu8(uint8_t u8) {
	printf("%hhx%hhx%hhx%hhx%hhx%hhx%hhx%hhx\n", (u8 >> 7) & 1, (u8 >> 6) & 1, (u8 >> 5) & 1, (u8 >> 4) & 1, (u8 >> 3) & 1, (u8 >> 2) & 1, (u8 >> 1) & 1, (u8 >> 0) & 1);
}

#if DEBUG == 1

void dbg() {
	const int c = 65;
	bitarray::BitArray ba(c);
	for (int i = 0; i < c; ++i) {
		ba.set(i, 1);
	}
	ba.p();
	ba.calcSums();
	printf("sc: %llu\n", (unsigned long long) ba.sum(c-1));
	for (int i = 1; i < c; i += 2) {
		ba.set(i, 0);
	}
	ba.p();
	ba.calcSums();
	printf("sc: %llu\n", (unsigned long long) ba.sum(c-1));
}
#endif

void usage(const char *argv0) {
	std::cout << "USAGE: " << argv0 << " <action> <arguments>" << std::endl;
	std::cout << "Actions:" << std::endl;
#if DEBUG == 1
	std::cout << "\t" << "debug" << std::endl;
	std::cout << "\t\t" << "Flavor of the hour development stuff" << std::endl;
#endif
	std::cout << "\t" << "bitarr <n> [<seed>]" << std::endl;
	std::cout << "\t\t" << "Does BitArray stuff. Optional seed parameter for repeatable results." << std::endl;
	std::cout << "\t" << "pkint  <n> <k> [<seed>]" << std::endl;
	std::cout << "\t\t" << "Does PackedIntegerArray stuff. Optional seed parameter for repeatable results." << std::endl;
}

int main(int argc, char **argv) {
	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	if (argc <= 2) {
		if (argc == 2) {
#if DEBUG == 1
			if (!strncmp("debug", argv[1], 6)) {
				dbg();
				return 0;
			}
#endif
		}
		std::cout << "Too few arguments supplied!" << std::endl;
		usage(argv[0]);
		return 1;
	}
	if (argc > 5) {
		std::cout << "Too many arguments supplied!" << std::endl;
		usage(argv[0]);
		return 1;
	}
	if (!strncmp("bitarr", argv[1], 7)) {
		uint64_t n, s = 0;
		if (argc > 4) {
			std::cout << "Too many arguments supplied!" << std::endl;
			usage(argv[0]);
			return 1;
		}
		try {
			n = std::stoull(argv[2]);
			if (argc == 4) {
				s = std::stoull(argv[3]);
			}
		} catch (...) {
			std::cout << "Failed to parse number parameter!" << std::endl;
			return 1;
		}
		int ret;
		if (argc == 4) {
			ret = action::bitarr(n, s);
		} else {
			ret = action::bitarr(n);
		}
		print_resource_report();
		return ret;
	}
	if (!strncmp("pkint", argv[1], 6)) {
		uint64_t n, k, s = 0;
		try {
			n = std::stoull(argv[2]);
			k = std::stoull(argv[3]);
			if (argc == 5) {
				s = std::stoull(argv[4]);
			}
		} catch (...) {
			std::cout << "Failed to parse number parameter!" << std::endl;
			return 1;
		}
		int ret;
		if (argc == 5) {
			ret = action::pkint(n, k, s);
		} else {
			ret = action::pkint(n, k);
		}
		print_resource_report();
		return ret;
	}
	std::cout << "Unknown action!" << std::endl;
	usage(argv[0]);
	return 1;
}
