#include "packedintegerarray.hpp"

using namespace packedintegerarray;

PackedIntegerArray::PackedIntegerArray(
	uint64_t n,
	uint64_t k
) :
	_n(n),
	_k(k),
	_mask(k < sizeof(_mask)*8 ? (1LL << k) - 1LL : ~0LL),
	_elems(1+(n*k)/64)
{
	this->_d = new uint64_t[this->_elems];
	for (uint64_t i = 0LL; i < this->_elems; ++i) {
		this->_d[i] = 0LL;
	}
}

PackedIntegerArray::~PackedIntegerArray() {
	delete [] this->_d;
}

uint64_t PackedIntegerArray::get(uint64_t i) {
	if (i > this->_n) {
		return 0LL;
	}
	uint64_t ret = 0LL;
	uint64_t bp = i * this->_k;
	uint64_t posl = bp / 64;
	uint64_t posh = (bp + this->_k - 1) / 64;
	uint64_t lc = this->_d[posl];
	ret |= (lc >> (bp % 64)) & this->_mask;
	if (posl < posh) {
		uint64_t rc = this->_d[posh];
		ret |= (rc << (64 - (bp % 64))) & this->_mask;
	}
	return ret;
}

void PackedIntegerArray::set(uint64_t i, uint64_t v) {
	if (i > this->_n || v > this->_mask) {
		return;
	}
	uint64_t bp = i * this->_k;
	uint64_t posl = bp / 64;
	uint64_t posh = (bp + this->_k - 1) / 64;
	this->_d[posl] &= ~(this->_mask << (bp % 64));
	this->_d[posl] |= (v & this->_mask) << (bp % 64);
	if (posl < posh) {
		this->_d[posh] &= ~(this->_mask >> (64 - (bp % 64)));
		this->_d[posh] |= (v & this->_mask) >> (64 - (bp % 64));
	}
}
