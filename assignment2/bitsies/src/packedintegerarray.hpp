#pragma once

#ifndef _PFP_PACKEDINTEGERARRAY_HPP
#define _PFP_PACKEDINTEGERARRAY_HPP

#include <cstdint>

namespace packedintegerarray {

class PackedIntegerArray {
private:
	const uint64_t _n;
	const uint64_t _k;
	const uint64_t _mask;
	const uint64_t _elems;
	uint64_t *_d;

public:
	PackedIntegerArray(uint64_t n, uint64_t k);
	~PackedIntegerArray();

	uint64_t get(uint64_t i);
	void set(uint64_t i, uint64_t v);
};

}

#endif // _PFP_PACKEDINTEGERARRAY_HPP
