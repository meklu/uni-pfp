Task 1
======

The time taken setting bits seems to be slower than the time taken getting them.
I suspect this is mainly due to the cache having to be invalidated for almost
every bit that is set and its associated writeback cost all the way through
the different cache levels and into main memory. This problem should be
exacerbated by the random nature of the accesses.
  With small enough m, the get cycle being executed after the set cycle also
benefits from the pages slammed into the cache by the set cycle by increasing
the cache hit to cache miss ratio.

A very negligible additional cost is also associated with the set() method I
implemented as it has one more conditional branch due to taking a value
parameter (|= vs. &= in the bit manipulation). This isn't significant enough
to show up in testing as one would expect.

For sufficiently low values of m, both get cycles seem to finish in roughly the
same time. For bigger values of m, this isn't the case anymore quite possibly
due to cache thrashing. The following is a description of what I think is
happening in those situations.
  After generating a new set of m numbers, the get operations take a
significantly longer amount of time than either the initial set or get
procedures. I suspect this is due to the cost of bringing into cache a bunch of
pages that haven't been used sufficiently recently resulting in them having
been replaced by the pages we looked at in the previous cycles. This is due to
those pages being replaced in cache by more recently or frequently used pages.
  There's also the fact that generating new indices for getting bits means that
with intermediate values of m the first get cycle will have benefitted from the
pages brought into cache by the set cycle but not having had to worry about all
the pages that didn't have any action during the set cycle. Regenerating the
indices for the second get cycle means that the pages accessed will be less
likely to be present in cache resulting in more cache misses and therefore more
memory fetches.


Task 2
======

Before taking a gander at what the mentioned gcc built-ins did, I had an idea
of precomputing the bit counts of singular bytes into a roughly 2^8 element
data structure. The most obvious way would be to just use a char[] whose index
would denote the byte being looked up and whose element value would simply be
the number of bits that said byte has that are set.
  There are a couple of neat observations that can be made though: the range of
values that are interesting are anything between [1,7], since only 0x0 and 0xFF
have a set bit count outside of this range. Since the value 7 only takes up
three bits, we could pack this into a packed integer array with chunks of 3
bits.
  Now we have a couple of options: we can make a fast path for 0x0 and
increment each fetched value by 1. We could also instead make the fast path
0xFF and just use each fetched value as-is. The third option would be to make
both fast paths and decrement the looked up index by 1 and use the fetched
value as is.
  Of these three options, I think the second one makes the most sense as it is
the least complex in terms of instruction count and only requires three
additional bits in comparison to the third option.
  A char[128] used as half-bytes without any of this fancy packing stuff would
probably still be faster and would take up only slightly more space in any
case. Even still I wouldn't be surprised if a char[256] were even faster than
that.

In the end, I wound up using the popcount built-in. The implementation for a
general non-pow2 value of t was kind of a pain and probably a bit silly in the
end. t is soft-hard-coded to 64 in src/bitarray.hpp and the implementation
can't use values greater than 64 for said parameter.


Task 3
======

Not much to say here.


Task 4
======

-
