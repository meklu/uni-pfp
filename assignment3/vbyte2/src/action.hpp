#pragma once

#ifndef _PFP_ACTION_HPP
#define _PFP_ACTION_HPP

#include <cstdint>
#include <string>

namespace action {

int vbseqbench(std::string infile, uint64_t k, uint64_t seed);
int vbseqbench(std::string infile, uint64_t k);

int vbseqlbench(std::string infile, uint64_t k, uint64_t seed);
int vbseqlbench(std::string infile, uint64_t k);

}

#endif // _PFP_ACTION_HPP
