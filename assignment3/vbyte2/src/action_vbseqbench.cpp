#include "action.hpp"
#include "bench.hpp"
#include "vbyte.hpp"

static inline uint64_t extract(const vbyte::VByteSeq *vb, uint64_t i) {
	return vb->accessScan(i);
}

static inline void infomagic(const vbyte::VByteSeq *vb) {
	std::cout <<
		"VByte sequence computed, obj size is " <<
		vb->getSizeInBytes() << " B" <<
		", PIA has " << vb->getPIA()->itemCount() << " items" <<
		" backed by " << vb->getPIA()->size() << " vector elements" <<
	std::endl;
}

int action::vbseqbench(std::string infile, uint64_t k, uint64_t seed) {
	std::cout << "Benching VByteSeqLayered" << std::endl;
	return bench<vbyte::VByteSeq>(infile, k, seed, extract, infomagic);
}

int action::vbseqbench(std::string infile, uint64_t k) {
	std::random_device rd;
	return action::vbseqbench(infile, k, rd());
}
