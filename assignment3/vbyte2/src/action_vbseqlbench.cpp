#include "action.hpp"
#include "bench.hpp"
#include "vbyte.hpp"

static inline uint64_t extract(const vbyte::VByteSeqLayered *vb, uint64_t i) {
	return vb->get(i);
}

static inline void infomagic(const vbyte::VByteSeqLayered *vb) {
	std::cout <<
		"Layered VByte sequence computed, obj size is " <<
		vb->getSizeInBytes() << " B" <<
		", has " << vb->getUsedLayers() << " layers in use" <<
	std::endl;
}

int action::vbseqlbench(std::string infile, uint64_t k, uint64_t seed) {
	std::cout << "Benching VByteSeqLayered" << std::endl;
	return bench<vbyte::VByteSeqLayered>(infile, k, seed, extract, infomagic);
}

int action::vbseqlbench(std::string infile, uint64_t k) {
	std::random_device rd;
	return action::vbseqlbench(infile, k, rd());
}
