#include <chrono>
#include <cstdio>
#include <iostream>
#include <random>
#include <vector>

#include "action.hpp"
#include "file.hpp"
#include "vbyte.hpp"

template <class T> void p_delta(
	const std::string & str,
	std::chrono::time_point<T> ts_start,
	std::chrono::time_point<T> ts_end
) {
	std::chrono::nanoseconds elapsed = ts_end - ts_start;
	// This is kind of ugly but there were some issues doing it supposedly nicely
	std::chrono::nanoseconds s = elapsed / 1000000000ULL;
	std::chrono::nanoseconds ns = elapsed - s;
	printf(
		"%s: %llu.%09llu s\n",
		str.c_str(),
		(unsigned long long) s.count(),
		(unsigned long long) ns.count()
	);
}

static inline void genrandvec(
	uint64_t m,
	std::vector<uint64_t> &v,
	std::mt19937_64 &gen,
	std::uniform_int_distribution<uint64_t> &dis
) {
	// generate random vector
	std::cout << "generating random vector (m: " << m << ")  ...";
	v.clear();
	v.resize(m);
	for (uint64_t i = 0; i < m; ++i) {
		uint64_t p = dis(gen);
		v[i] = p;
	}
	std::cout << "  done" << std::endl;
}

template <class T> static inline int bench(
	std::string infile, uint64_t k,
	uint64_t seed,
	uint64_t (*extractor) (const T *vb, uint64_t i),
	void (*infomagic) (const T *vb)
) {
	// parse args
	if (!k) {
		std::cout << "k can't be 0!" << std::endl;
		return 1;
	}
	if (k > 63) {
		std::cout << "k is too big! (" << k << " > 63" << std::endl;
		return 1;
	}

	std::ifstream ifs = file::in(infile);
	if (!ifs.is_open()) {
		std::cout << "Couldn't open input file!" << std::endl;
		return 1;
	}

	uint64_t n = 0LL;
	T *vb = NULL;
	{
		// we're creating the vector here to try and coax it to be
		// deallocated after exiting this sub-block
		std::cout << "Reading input file from '" << infile << "'" << std::endl;
		std::vector<uint64_t> ns = file::read_full_u64(ifs);
		n = ns.size();
		std::cout <<
			"Constructing VByte sequence (n: " << n <<
			", k: " << k << ")" <<
		std::endl;
		vb = new T(ns.data(), ns.size(), k);
		if (ns.size() != vb->getN()) {
			std::cout << "Size mismatch between input file and generated VByte sequence!" << std::endl;
			delete vb;
			return 1;
		}
	}

	if (vb == NULL) {
		std::cout << "Failed to allocate VByte sequence!" << std::endl;
		return 1;
	}
	infomagic(vb);

	// actual benchmark
	std::mt19937_64 gen(seed);
	uint64_t max = n - 1;
	std::uniform_int_distribution<uint64_t> dis(0, max);
	// number of random accesses
	uint64_t ms[] = {1, 10, 100, 1000, 10000, 100000, 1000000};
	std::vector<uint64_t> v;

	// generate random vector
	// calculate scan duration
	std::cout << "performing scans" << std::endl;
	for (uint64_t m : ms) {
		genrandvec(m, v, gen, dis);
		auto ts_start = std::chrono::steady_clock::now();
		for (uint64_t i = 0; i < m; ++i) {
			extractor(vb, v[i]);
		}
		auto ts_end = std::chrono::steady_clock::now();
		p_delta("  scan", ts_start, ts_end);
	}

	delete vb;
	return 0;
}
