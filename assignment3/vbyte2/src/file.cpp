#include "file.hpp"

std::ofstream file::out(const std::string & filename) {
	std::ofstream file;
	file.open(filename.c_str(), std::ios::out | std::ios::trunc | std::ios::binary);
	return file;
}

std::ifstream file::in(const std::string & filename) {
	std::ifstream file;
	file.open(filename.c_str(), std::ios::in | std::ios::binary);
	return file;
}

std::vector<uint64_t> file::read_full_u64(std::ifstream & f) {
	int64_t sz;
	f.seekg(0, std::ios_base::end);
	sz = f.tellg();
	f.seekg(0, std::ios_base::beg);
	uint64_t *buf = new uint64_t[sz/8];
	f.read((char *) buf, (sz/8)*8);
	std::vector<uint64_t> ret = std::vector<uint64_t>(sz/8);
	for (int64_t i = 0; i < sz/8; ++i) {
		ret[i] = buf[i];
	}
	delete[] buf;
	return ret;
}

std::vector<uint8_t> file::read_full_u8(std::ifstream & f) {
	int64_t sz;
	f.seekg(0, std::ios_base::end);
	sz = f.tellg();
	f.seekg(0, std::ios_base::beg);
	uint8_t *buf = new uint8_t[sz];
	f.read((char *) buf, sz);
	std::vector<uint8_t> ret = std::vector<uint8_t>(sz);
	for (int64_t i = 0; i < sz; ++i) {
		ret[i] = buf[i];
	}
	delete[] buf;
	return ret;
}
