#include <iostream>
#include <cstring>

#include <cerrno>
#include <sys/time.h>
#include <sys/resource.h>

#include "action.hpp"
#include "vbyte.hpp"

struct timespec ts_start;

void print_resource_report() {
	struct rusage r;
	struct timespec ts_end;
	errno = 0;
	if (0 > getrusage(RUSAGE_SELF, &r)) {
		std::cout << "getrusage failed: " << strerror(errno) << std::endl;
		return;
	}
	clock_gettime(CLOCK_MONOTONIC, &ts_end);
	ts_end.tv_nsec -= ts_start.tv_nsec;
	if (ts_end.tv_nsec < 0) {
		ts_end.tv_nsec += 1000000000;
		ts_end.tv_sec -= 1;
	}
	ts_end.tv_sec -= ts_start.tv_sec;
	printf("\n--- resource usage ---\n");
	printf("max rss: %ldK\n", r.ru_maxrss);
	printf("wtime  : %lld.%09lld\n",
		(long long) ts_end.tv_sec,
		(long long) ts_end.tv_nsec
	);
	printf("utime  : %lld.%06lld\n", (long long) r.ru_utime.tv_sec, (long long) r.ru_utime.tv_usec);
	printf("stime  : %lld.%06lld\n", (long long) r.ru_stime.tv_sec, (long long) r.ru_stime.tv_usec);
}

void pu8(uint8_t u8) {
	printf("%hhx%hhx%hhx%hhx%hhx%hhx%hhx%hhx\n", (u8 >> 7) & 1, (u8 >> 6) & 1, (u8 >> 5) & 1, (u8 >> 4) & 1, (u8 >> 3) & 1, (u8 >> 2) & 1, (u8 >> 1) & 1, (u8 >> 0) & 1);
}

#if DEBUG == 1

void dbg() {
	uint64_t ns[] = {3, 15, 500, 200, 18, 0};
	uint64_t sz = sizeof(ns) / sizeof(ns[0]);
	auto *vb = new vbyte::VByteSeqLayered(ns, sz, 4);
	std::cout << "vb->getSizeInBytes(): " << vb->getSizeInBytes() << std::endl;
	vb->p();
	for (uint64_t i = 0; i < sz; ++i) {
		std::cout << "i(" << i << "): " << vb->get(i) << std::endl;
	}
	delete vb;
}
#endif

void usage(const char *argv0) {
	std::cout << "USAGE: " << argv0 << " <action> <arguments>" << std::endl;
	std::cout << "Actions:" << std::endl;
#if DEBUG == 1
	std::cout << "\t" << "debug" << std::endl;
	std::cout << "\t\t" << "Flavor of the hour development stuff" << std::endl;
#endif
	std::cout << "\t" << "vbseqbench <file> <k> [<seed>]" << std::endl;
	std::cout << "\t\t" << "Benchmarks VByte sequences where k is as specified. Optional seed parameter for repeatable results." << std::endl;
	std::cout << "\t" << "vbseqlbench <file> <k> [<seed>]" << std::endl;
	std::cout << "\t\t" << "Benchmarks layered VByte sequences where k is as specified. Optional seed parameter for repeatable results." << std::endl;
}

int main(int argc, char **argv) {
	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	if (argc <= 2) {
		if (argc == 2) {
#if DEBUG == 1
			if (!strncmp("debug", argv[1], 6)) {
				dbg();
				return 0;
			}
#endif
		}
		std::cout << "Too few arguments supplied!" << std::endl;
		usage(argv[0]);
		return 1;
	}
	if (argc > 5) {
		std::cout << "Too many arguments supplied!" << std::endl;
		usage(argv[0]);
		return 1;
	}
	if (!strncmp("vbseqbench", argv[1], 11) || !strncmp("vbseqlbench", argv[1], 12)) {
		if (argc < 4) {
			std::cout << "Too few arguments supplied!" << std::endl;
			usage(argv[0]);
			return 1;
		}
		std::string infile = argv[2];
		uint64_t k, s = 0;
		try {
			k = std::stoull(argv[3]);
			if (argc == 5) {
				s = std::stoull(argv[4]);
			}
		} catch (...) {
			std::cout << "Failed to parse number parameter!" << std::endl;
			return 1;
		}
		int ret;
		if (!strncmp("vbseqbench", argv[1], 11)) {
			if (argc == 5) {
				ret = action::vbseqbench(infile, k, s);
			} else {
				ret = action::vbseqbench(infile, k);
			}
		} else {
			if (argc == 5) {
				ret = action::vbseqlbench(infile, k, s);
			} else {
				ret = action::vbseqlbench(infile, k);
			}
		}
		print_resource_report();
		return ret;
	}
	std::cout << "Unknown action!" << std::endl;
	usage(argv[0]);
	return 1;
}
