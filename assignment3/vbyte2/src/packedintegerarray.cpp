#include "packedintegerarray.hpp"
#include <iostream>

using namespace packedintegerarray;

PackedIntegerArray::PackedIntegerArray(
	uint64_t k
) :
	_k(k),
	_mask(k < sizeof(_mask)*8 ? (1LL << k) - 1LL : ~0LL),
	_ic(0)
{
}

PackedIntegerArray::~PackedIntegerArray() {
}

uint64_t PackedIntegerArray::get(uint64_t i) const {
	if ((i + 1) * this->_k > this->_d.size() * 64) {
		return 0LL;
	}
	uint64_t ret = 0LL;
	uint64_t bp = i * this->_k;
	uint64_t posl = bp / 64;
	uint64_t posh = (bp + this->_k - 1) / 64;
	uint64_t lc = this->_d[posl];
	ret |= (lc >> (bp % 64)) & this->_mask;
	if (posl < posh) {
		uint64_t rc = this->_d[posh];
		ret |= (rc << (64 - (bp % 64))) & this->_mask;
	}
	return ret;
}

void PackedIntegerArray::set(uint64_t i, uint64_t v) {
	if (v > this->_mask) {
		return;
	}
	// Shouldn't be needed after new prealloc stuff
	while ((i + 1) * this->_k > this->_d.size() * 64) {
#if DEBUG == 1
		std::cout << "PackedIntegerArray::set(): realloc" << std::endl;
#endif
		this->_d.push_back(0LL);
	}
	if (i+1 > this->_ic) {
		this->_ic = i+1;
	}
	uint64_t bp = i * this->_k;
	uint64_t posl = bp / 64;
	uint64_t posh = (bp + this->_k - 1) / 64;
	this->_d[posl] &= ~(this->_mask << (bp % 64));
	this->_d[posl] |= (v & this->_mask) << (bp % 64);
	if (posl < posh) {
		this->_d[posh] &= ~(this->_mask >> (64 - (bp % 64)));
		this->_d[posh] |= (v & this->_mask) >> (64 - (bp % 64));
	}
}

uint64_t PackedIntegerArray::size() const {
	return this->_d.size();
}

uint64_t PackedIntegerArray::itemCount() const {
	return this->_ic;
}

std::vector<uint64_t> * PackedIntegerArray::getV() {
	return &(this->_d);
}

uint64_t PackedIntegerArray::getSizeInBytes() const {
	uint64_t ret = sizeof(*this);
	// best estimate for backing data structure
	ret += sizeof(uint64_t) * this->_d.capacity();
	return ret;
}
