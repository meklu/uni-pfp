#pragma once

#ifndef _PFP_PACKEDINTEGERARRAY_HPP
#define _PFP_PACKEDINTEGERARRAY_HPP

#include <cstdint>
#include <vector>

namespace packedintegerarray {

class PackedIntegerArray {
private:
	const uint64_t _k;
	const uint64_t _mask;
	uint64_t _ic;
	std::vector<uint64_t> _d;

public:
	PackedIntegerArray(uint64_t k);
	~PackedIntegerArray();

	uint64_t get(uint64_t i) const;
	void set(uint64_t i, uint64_t v);

	uint64_t size() const;
	uint64_t itemCount() const;

	std::vector<uint64_t> *getV();

	// Returns the size of this object
	uint64_t getSizeInBytes() const;
};

}

#endif // _PFP_PACKEDINTEGERARRAY_HPP
