#pragma once

#ifndef _PFP_VBYTE_HPP
#define _PFP_VBYTE_HPP

#include <cstdint>
#include "bitarray.hpp"
#include "packedintegerarray.hpp"

namespace vbyte {

// gives the number of segments of k bits that an integer will be split in
inline static uint64_t countParts(uint64_t k, uint64_t encodable) {
	if (!encodable) { return 1ULL; }
	return ((64ULL - __builtin_clzll(encodable) - 1ULL) / k) + 1ULL;
}

class VByteSeq {
private:
	const uint64_t _n;
	const uint8_t _k;
	packedintegerarray::PackedIntegerArray *_d;

public:
	VByteSeq(const uint64_t ns[], uint64_t sz, uint8_t k);
	~VByteSeq();

	uint64_t accessScan(uint64_t i) const;

	uint64_t getSizeInBytes() const;
	uint64_t getN() const;
	const packedintegerarray::PackedIntegerArray *getPIA() const;

	void p() const;
};

class VByteSeqLayered {
private:
	const uint64_t _n;
	const uint64_t _maxLayers;
	const uint8_t _k;
	bitarray::BitArray **_b;
	packedintegerarray::PackedIntegerArray **_d;

	uint64_t _usedLayers;

	uint64_t calcPosInLayer(const uint64_t layer, const uint64_t i) const;

public:
	VByteSeqLayered(const uint64_t ns[], uint64_t sz, uint8_t k);
	~VByteSeqLayered();

	uint64_t get(uint64_t i) const;

	uint64_t getSizeInBytes() const;
	uint64_t getN() const;

	uint64_t getUsedLayers() const;

	void p() const;
};

}

#endif // _PFP_VBYTE_HPP
