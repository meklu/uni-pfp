#include "vbyte.hpp"
#include <iostream>

using namespace vbyte;

VByteSeq::VByteSeq(const uint64_t ns[], uint64_t sz, uint8_t k) :
	_n(sz),
	_k(k),
	_d(new packedintegerarray::PackedIntegerArray(k+1))
{
	uint64_t pos = 0;
	// Predetermine needed allocation
	// This is much cheaper than resizing on the fly seven trillion times
	uint64_t parts = 0;
	for (uint64_t i = 0; i < sz; ++i) {
		parts += countParts(k, ns[i]);
	}
	this->_d->getV()->resize((parts * (k+1) - 1) / 64 + 1);
	for (uint64_t i = 0; i < sz; ++i) {
		uint64_t enc = ns[i];
		uint64_t ls = 0;
		for (;;) {
			uint64_t p = pos;
			++pos;
			ls = enc & ((1LL << this->_k) - 1LL);
			enc >>= this->_k;
			if (enc == 0) {
				// set the stop bit
				this->_d->set(p, ls | (1LL << this->_k));
				break;
			}
			this->_d->set(p, ls);
		}
	}
	// Shouldn't be needed after new prealloc stuff
	{
		if (this->_d->getV()->capacity() != this->_d->getV()->size()) {
#if DEBUG == 1
			std::cout << "VByteSeq::VByteSeq(): shrinking backing vector" << std::endl;
#endif
			this->_d->getV()->shrink_to_fit();
		}
	}
}

VByteSeq::~VByteSeq() {
	delete this->_d;
}

uint64_t VByteSeq::accessScan(uint64_t i) const {
	uint64_t pos = 0;
	uint64_t dec = 0;
	for (uint64_t count = 0; count < i; ++pos) {
		uint64_t ck = this->_d->get(pos);
		if (ck >> this->_k) {
			++count;
		}
	}
	for (uint64_t j = 0;; ++j, ++pos) {
		uint64_t ck = this->_d->get(pos);
		uint64_t sign = ck & (1LL << this->_k);
		ck ^= sign;
		dec |= ck << (j * this->_k);
		if (sign) {
			break;
		}
	}
	return dec;
}

void VByteSeq::p() const {
	const uint64_t ic = this->_d->itemCount();
	std::cout << "VByteSeq::p(): PIA->itemCount(): " << ic << std::endl;
	for (uint64_t i = 0;i < ic; ++i) {
		uint64_t e = this->_d->get(i);
		for (uint8_t j = 0; j <= this->_k; ++j) {
			std::cout << (e >> ((this->_k - j))&1);
		}
		std::cout << std::endl;
	}
}

uint64_t VByteSeq::getSizeInBytes() const {
	return (
		// the size of our VByte object
		sizeof(*this) +
		// the size of the PackedIntegerArray
		sizeof(*(this->_d)) +
		// best estimate for the vector backing our PIA
		this->_d->getSizeInBytes()
	);
}

uint64_t VByteSeq::getN() const {
	return this->_n;
}

const packedintegerarray::PackedIntegerArray *VByteSeq::getPIA() const {
	return this->_d;
}
