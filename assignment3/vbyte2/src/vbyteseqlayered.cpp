#include "vbyte.hpp"
#include <iostream>

using namespace vbyte;

uint64_t VByteSeqLayered::calcPosInLayer(const uint64_t layer, const uint64_t i) const {
	uint64_t posInLayer = i;
	for (uint64_t l = 0; l < layer; ++l) {
		posInLayer = posInLayer - this->_b[l]->sum(posInLayer);
	}
	return posInLayer;
}

VByteSeqLayered::VByteSeqLayered(const uint64_t ns[], uint64_t sz, uint8_t k) :
	_n(sz),
	_maxLayers(countParts(k, ~0ULL)),
	_k(k)
{
	this->_b = new bitarray::BitArray*[this->_maxLayers];
	this->_d = new packedintegerarray::PackedIntegerArray*[this->_maxLayers];

	this->_usedLayers = 0;

	for (uint64_t i = 0; i < this->_maxLayers; ++i) {
		this->_b[i] = NULL;
		this->_d[i] = NULL;
	}

	uint64_t layerSize = this->_n;
	uint64_t elemsProcessed = 0;
	for (uint64_t layer = 0; layer < this->_maxLayers && elemsProcessed < this->_n; ++layer) {
		this->_b[layer] = new bitarray::BitArray(layerSize);
		this->_d[layer] = new packedintegerarray::PackedIntegerArray(this->_k);
		this->_d[layer]->getV()->resize((layerSize * k - 1) / 64 + 1);

		for (uint64_t i = 0; i < this->_n && elemsProcessed < this->_n; ++i) {
			uint64_t enc = ns[i];
			uint64_t parts = countParts(this->_k, enc);
			if (parts <= layer) {
				continue;
			}
			enc >>= layer * this->_k;
			uint64_t ls = enc & ((1LL << this->_k) - 1LL);
			// calculate position in layer
			uint64_t posInLayer = this->calcPosInLayer(layer, i);
			// insert stop bit here
			if (parts == layer + 1) {
				++elemsProcessed;
				this->_b[layer]->set(posInLayer, 1);
			}
			this->_d[layer]->set(posInLayer, ls);
		}
		++this->_usedLayers;
		this->_b[layer]->calcSums();
		layerSize = layerSize - this->_b[layer]->sum(layerSize - 1);
	}
}

VByteSeqLayered::~VByteSeqLayered() {
	for (uint64_t i = 0; i < this->_maxLayers; ++i) {
		if (this->_b[i]) {
			delete this->_b[i];
		}
		if (this ->_d[i]) {
			delete this->_d[i];
		}
	}
	delete [] this->_b;
	delete [] this->_d;
}

uint64_t VByteSeqLayered::get(uint64_t i) const {
	uint64_t pos = 0;
	uint64_t dec = 0;
	for (uint64_t layer = 0; layer < this->_maxLayers; ++layer, ++pos) {
		uint64_t posInLayer = this->calcPosInLayer(layer, i);
		uint64_t part = this->_d[layer]->get(posInLayer);
		dec |= part << (pos * this->_k);
		if (this->_b[layer]->get(posInLayer)) {
			break;
		}
	}
	return dec;
}

void VByteSeqLayered::p() const {
	for (uint64_t layer = 0; layer < this->_maxLayers; ++layer) {
		if (!this->_b[layer]) { break; }
		std::cout << "[L" << layer << "] ";
		this->_b[layer]->p();
		if (!this->_d[layer]) { break; }
		const uint64_t ic = this->_d[layer]->itemCount();
		std::cout << "[L" << layer << "] PIA->itemCount(): " << ic << std::endl;
		for (uint64_t i = 0;i < ic; ++i) {
			std::cout << "[L" << layer << "] ";
			uint64_t e = this->_d[layer]->get(i);
			for (uint8_t j = 1; j <= this->_k; ++j) {
				std::cout << (e >> ((this->_k - j))&1);
			}
			std::cout << std::endl;
		}
	}
}

uint64_t VByteSeqLayered::getSizeInBytes() const {
	uint64_t ret = sizeof(*this);
	for (uint64_t i = 0; i < this->_maxLayers; ++i) {
		ret += !this->_b[i] ? 0ULL : this->_b[i]->getSizeInBytes();
		ret += !this->_d[i] ? 0ULL : this->_d[i]->getSizeInBytes();
	}
	return ret;
}

uint64_t VByteSeqLayered::getN() const {
	return this->_n;
}

uint64_t VByteSeqLayered::getUsedLayers() const {
	return this->_usedLayers;
}
